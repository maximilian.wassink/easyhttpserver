#!/usr/bin/env bash

echo "clonig source files..."
git clone https://gitlab.com/maximilian.wassink/easyhttpserver.git
echo "cloned source files!"
echo "starting maven goals..."
cd easyhttpserver
mvn install
cd ..
cp easyhttpserver/target/EasyHttpServer-1.0-SNAPSHOT.jar EasyHttpServer.jar
rm -r easyhttpserver
echo "finished installation"